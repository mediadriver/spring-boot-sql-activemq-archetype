#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class AppProperties {

	// AcrtiveMQ Properties
	@Value("${symbol_dollar}{amq.broker.url}")
	private String brokerUrl;

	@Value("${symbol_dollar}{amq.userName}")
	private String userName;

	@Value("${symbol_dollar}{amq.password}")
	private String password;
	
	@Value("${symbol_dollar}{amq.destination}")
	private String destination;

	// DataSource Properties
	@Value("${symbol_dollar}{dataSource.driver.className}")
	private String dataSourceDriverClassName;
	
	@Value("${symbol_dollar}{dataSource.url}")
	private String dataSourceUrl;
	
	@Value("${symbol_dollar}{dataSource.userName}")
	private String dataSourceUserName;
	
	@Value("${symbol_dollar}{dataSource.password}")
	private String dataSourcePassword;

	public String getBrokerUrl() {
		return brokerUrl;
	}

	public void setBrokerUrl(String brokerUrl) {
		this.brokerUrl = brokerUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDataSourceDriverClassName() {
		return dataSourceDriverClassName;
	}

	public void setDataSourceDriverClassName(String dataSourceDriverClassName) {
		this.dataSourceDriverClassName = dataSourceDriverClassName;
	}

	public String getDataSourceUrl() {
		return dataSourceUrl;
	}

	public void setDataSourceUrl(String dataSourceUrl) {
		this.dataSourceUrl = dataSourceUrl;
	}

	public String getDataSourceUserName() {
		return dataSourceUserName;
	}

	public void setDataSourceUserName(String dataSourceUserName) {
		this.dataSourceUserName = dataSourceUserName;
	}

	public String getDataSourcePassword() {
		return dataSourcePassword;
	}

	public void setDataSourcePassword(String dataSourcePassword) {
		this.dataSourcePassword = dataSourcePassword;
	}
}
